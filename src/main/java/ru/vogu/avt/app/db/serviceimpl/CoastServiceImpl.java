package ru.vogu.avt.app.db.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.SqlPlan;
import ru.vogu.avt.app.db.services.CoastService;

import java.util.List;

@Repository
public class CoastServiceImpl implements CoastService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<SqlPlan> getPlan() {
        return jdbcTemplate.query("select * from plan", new BeanPropertyRowMapper<>(SqlPlan.class));
    }
}
