"use strict";
import React from "react";
import ReactDOM from "react-dom";

import Application from "./customComponents/Application";
import MuiThemeProvider from "@material-ui/core/es/styles/MuiThemeProvider";
import AuthExample from "./customComponents/AuthExample";


const MaterialUI = () => (
    <MuiThemeProvider>
        <Application/>
    </MuiThemeProvider>
);
ReactDOM.render(
    <MaterialUI/>,
    document.getElementById('index')
);

