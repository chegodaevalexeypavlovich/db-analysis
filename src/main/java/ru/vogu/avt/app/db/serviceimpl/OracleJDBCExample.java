package ru.vogu.avt.app.db.serviceimpl;

import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.db.mapping.Mapper;
import ru.vogu.avt.app.SqlPlan;
import ru.vogu.avt.app.db.services.PersonService;

import java.sql.*;
import java.util.List;

public class OracleJDBCExample {
    private String exampleRequest = "select * from test";
    private String selectSQLId = String.format("Select sql_id from v$sql where sql_fulltext like '%s'", exampleRequest);
    private String selectSQLPlan = "Select * from v$sql_plan where sql_id='";

    public List<Person> getPlanForSQLRequest() {
        List<SqlPlan> sqlPlans = null;

        Connection connection = getConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSQLId);
            resultSet.next();
            String sql_id = resultSet.getString("SQL_ID");
            String s = selectSQLPlan + sql_id + "'";
            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(s);

            sqlPlans = Mapper.convert(resultSet1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Connection getConnection() {
        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();

        }

        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:orcl", "alex", "1234");

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();

        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return connection;
    }

}