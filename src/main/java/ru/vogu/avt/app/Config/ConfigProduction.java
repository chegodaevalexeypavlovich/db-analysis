package ru.vogu.avt.app.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.vogu.avt.app.db.serviceimpl.OracleJDBCExample;
import ru.vogu.avt.app.db.services.PersonService;

@Configuration
@ComponentScan("ru.vogu.avt.app")
@Profile("production")
public class ConfigProduction {

    @Bean
    public PersonService personService() {
//        return new OracleJDBCExample();
        return null;
    }
}
