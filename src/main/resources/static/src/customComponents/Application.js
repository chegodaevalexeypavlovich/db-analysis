import CustomTable from "./CustomTable";
import React from "react";
import Button from '@material-ui/core/Button';
import CoastService from "../services/CoastService";


export default class Application extends React.Component {
    constructor() {
        super();
        this.state = {
            cost: []
        };
        this.handleClick = this.handleClick.bind(this);
    };

    handleClick() {
        console.log("CoastService.getCoast(this);\n");
        CoastService.getCoast(this);
        console.log(this.state.cost);
    };

    render() {
        return (
            <div>
                <Button onClick={this.handleClick}>
                    Update
                </Button>
                <CustomTable cost={this.state.cost}/>
            </div>
        );
    };
};


