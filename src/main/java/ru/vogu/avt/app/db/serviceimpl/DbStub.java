package ru.vogu.avt.app.db.serviceimpl;

import org.springframework.stereotype.Service;
import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.SqlPlan;
import ru.vogu.avt.app.db.services.PersonService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class DbStub  {
    public List<Person> getPlanForSQLRequest() {
        List<SqlPlan> sqlPlans = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 3; i++) {

            SqlPlan plan = new SqlPlan();
            plan.setCardinality(String.valueOf(random.nextInt(100) + 10));
            plan.setSqlId(String.valueOf(random.nextInt(100) + 10));
            plan.setTimestamp(String.valueOf(random.nextInt(100) + 10));
            plan.setOperation("SELECT STATEMENT");
            plan.setObjectName("setObjectName");
            plan.setObjectType("setObjectType");
            plan.setObjectType("setObjectType");
            plan.setCost(String.valueOf(random.nextInt(200) + 10));
            plan.setCardinality(String.valueOf(random.nextInt(200) + 10));
            plan.setCpuCost(String.valueOf(random.nextInt(20) + 10));
            plan.setIoCost(String.valueOf(random.nextInt(20) + 10));
            sqlPlans.add(plan);
        }
        return null;

    }
}
