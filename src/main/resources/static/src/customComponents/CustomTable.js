"use strict";
import React from 'react';

import CostTableRowColumn from "./CostTableRowColumn";
import Paper from "@material-ui/core/es/Paper/Paper";
import Table from "@material-ui/core/es/Table/Table";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import CoastService from "../services/CoastService";
import TableBody from "@material-ui/core/es/TableBody/TableBody";


export default class CustomTable extends React.Component {

    constructor() {
        super();
        this.state = {
            cost: []
        };

        CoastService.sendRequest('http://localhost:3000//db-analysis/rest/coast/get').then(resolve => {
            this.setState({cost: JSON.parse(resolve)})
        }, reject => console.log(reject));
    };


    render() {
        if (this.props.cost.length > 0) {
            this.state.cost = this.props.cost;
        }
        let cost = this.state.cost;
        console.log("Custom Table:");
        console.log(cost);
        let newsTemplate;
        if (cost.length) {
            newsTemplate = cost.map(function (item, index) {
                return (
                    <CostTableRowColumn cost={item} key={index}/>
                );
            });
        }

        return (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>sqlId</TableCell>
                            <TableCell>timesTamp</TableCell>
                            <TableCell>operation</TableCell>
                            <TableCell>cost</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {newsTemplate}
                    </TableBody>
                </Table>
            </Paper>
        );
    }

};

