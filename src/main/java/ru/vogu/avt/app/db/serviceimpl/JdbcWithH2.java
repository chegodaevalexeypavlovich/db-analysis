package ru.vogu.avt.app.db.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.db.services.PersonService;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class JdbcWithH2 implements PersonService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Person> getPlanForSQLRequest() {

        return jdbcTemplate.query("select * from person", new BeanPropertyRowMapper<>(Person.class));

    }

    @Override
    public Person findByUsername(String name) {
        return jdbcTemplate.queryForObject("select * from person WHERE name=?", new Object[]{name}, new BeanPropertyRowMapper<>(Person.class));
    }

    @Override
    public Person save(Person person) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO person( name, password) VALUES(?,?)";
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, person.getName());
                    ps.setString(2, person.getPassword());
                    return ps;
                }, keyHolder);


//        jdbcTemplate.execute("INSERT INTO person(name, password) VALUES(\'" + person.getName() + "\',\'" + person.getPassword() + "\')");
        person.setId((Integer) keyHolder.getKey());
        return person;
    }
}
