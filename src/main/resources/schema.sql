CREATE TABLE IF NOT EXISTS PERSON (
  ID       INT PRIMARY KEY AUTO_INCREMENT,
  NAME     VARCHAR(255),
  PASSWORD VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS PLAN (
  ID       INT PRIMARY KEY AUTO_INCREMENT,
  sqlId     VARCHAR(255),
  timesTamp     VARCHAR(255),
  operation     VARCHAR(255),
  objectName     VARCHAR(255),
  objectType     VARCHAR(255),
  cost     VARCHAR(255),
  cardinality     VARCHAR(255),
  cpuCost     VARCHAR(255),
  ioCost     VARCHAR(255),
);

INSERT INTO PLAN (sqlId, timesTamp, operation, objectName, objectType, cost, cardinality, cpuCost, ioCost)
VALUES(1,2,'select','test',1234,123123,123123,5,12312);

INSERT INTO person (NAME, PASSWORD) VALUES ('a', '$2a$10$w/TYET/UV2wFbC3Ue/Z5nuM.kBwUJil1boZbbBw4n8hyYWXnXm/jG');

