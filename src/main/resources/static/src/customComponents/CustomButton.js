/**
 * Created by sbt-chegodayev-ap on 26.06.2018.
 */
import React from 'react';

let buttonStyle = {
    margin: '10px 10px 10px 0'
};
export default class CustomButton extends React.Component{
    constructor() {
        super();
        this.state = {
            cost: []
        };

    };

    render(){
        return(
            <button className="btn btn-default"
                    style={buttonStyle}
                    onClick={this.props.handleClick}>{this.props.children}</button>
        )
    }

}
