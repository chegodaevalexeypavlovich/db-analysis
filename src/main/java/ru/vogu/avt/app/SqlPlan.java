package ru.vogu.avt.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SqlPlan {
    @Id
    @GeneratedValue
    private String id;
    private String sqlId;
    private String timesTamp;
    private String operation;
    private String objectName;
    private String objectType;
    private String cost;
    private String cardinality;
    private String cpuCost;
    private String ioCost;

    public SqlPlan() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTimesTamp(String timesTamp) {
        this.timesTamp = timesTamp;
    }

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public String getTimesTamp() {
        return timesTamp;
    }

    public void setTimestamp(String timesTamp) {
        this.timesTamp = timesTamp;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCardinality() {
        return cardinality;
    }

    public void setCardinality(String cardinality) {
        this.cardinality = cardinality;
    }

    public String getCpuCost() {
        return cpuCost;
    }

    public void setCpuCost(String cpuCost) {
        this.cpuCost = cpuCost;
    }

    public String getIoCost() {
        return ioCost;
    }

    public void setIoCost(String ioCost) {
        this.ioCost = ioCost;
    }
}
