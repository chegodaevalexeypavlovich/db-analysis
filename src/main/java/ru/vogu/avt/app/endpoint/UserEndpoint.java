package ru.vogu.avt.app.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.db.services.PersonService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("user")
public class UserEndpoint {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Person getCost(@RequestBody Person person) {
//        Person person = new Person();
        person.setPassword(bCryptPasswordEncoder.encode(person.getPassword()));
        return personService.save(person);
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String get(HttpServletRequest request) {
        return "hello";
    }


}
