package ru.vogu.avt.app.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.db.services.CoastService;
import ru.vogu.avt.app.db.services.PersonService;
import ru.vogu.avt.app.SqlPlan;

import java.util.List;

@RestController
@RequestMapping("coast")
public class CoastEndpoint {
    @Autowired
    CoastService coastService;

    @RequestMapping("/get")
    public List<SqlPlan> getCost() {
        return coastService.getPlan();
    }
}
