package ru.vogu.avt.app.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.vogu.avt.app.db.serviceimpl.CoastServiceImpl;
import ru.vogu.avt.app.db.serviceimpl.JdbcWithH2;
import ru.vogu.avt.app.db.services.CoastService;
import ru.vogu.avt.app.db.services.PersonService;

@Configuration
@ComponentScan("ru.vogu.avt.app")
@Profile("dev")
public class ConfigDev {

    @Bean
    public PersonService personService() {
        return new JdbcWithH2();
    }

    @Bean
    public CoastService coastService(){
        return new CoastServiceImpl();
    }
}
