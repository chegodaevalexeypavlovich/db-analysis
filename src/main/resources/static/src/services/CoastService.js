export default class CoastService {

    static sendRequest(url) {
        return new Promise(function (success, fail) {

            const request = new XMLHttpRequest();
            request.open('GET', url, true);

            request.addEventListener('load', () => {
                request.status >= 200 && request.status < 400
                    ? success(request.responseText)
                    : fail(new Error(`Request Failed: ${request.statusText}`));
            });

            request.addEventListener('error', () => {
                fail(new Error('Network Error'));
            });

            request.send();
        });
    }

    static getCoast(context){
        this.sendRequest('http://localhost:3000/db-analysis/rest/coast/get').then(resolve => {
            context.setState({cost: JSON.parse(resolve)})
        }, reject => console.log(reject));
    }
};