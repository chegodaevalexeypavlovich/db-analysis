package ru.vogu.avt.app.db.mapping;

import ru.vogu.avt.app.SqlPlan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Mapper {
    public static  List<SqlPlan> convert(ResultSet rs) throws SQLException {

        List<SqlPlan> sqlPlans = new ArrayList<>();

        while (rs.next()) {
            SqlPlan sqlPlan = new SqlPlan();
            sqlPlan.setSqlId(rs.getString("SQL_ID"));
            sqlPlan.setTimestamp(rs.getString("TIMESTAMP"));
            sqlPlan.setOperation(rs.getString("OPERATION"));
            sqlPlan.setObjectName(rs.getString("OBJECT_NAME"));
            sqlPlan.setObjectType(rs.getString("OBJECT_TYPE"));
            sqlPlan.setCost(rs.getString("COST"));
            sqlPlan.setCardinality(rs.getString("CARDINALITY"));
            sqlPlan.setCpuCost(rs.getString("CPU_COST"));
            sqlPlan.setIoCost(rs.getString("IO_COST"));
            sqlPlans.add(sqlPlan);
        }

        return sqlPlans;
    }
}
