package ru.vogu.avt.app.db.services;

import ru.vogu.avt.app.Entity.Person;
import ru.vogu.avt.app.SqlPlan;

import java.util.List;

public interface PersonService {
    List<Person> getPlanForSQLRequest();

    Person findByUsername(String name);

    Person save(Person person);
}
