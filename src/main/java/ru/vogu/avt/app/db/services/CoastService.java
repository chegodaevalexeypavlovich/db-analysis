package ru.vogu.avt.app.db.services;

import ru.vogu.avt.app.SqlPlan;

import java.util.List;

public interface CoastService {
    List<SqlPlan> getPlan();
}
