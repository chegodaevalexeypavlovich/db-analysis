import React from 'react';
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";


export default class CostTableRowColumn extends React.Component {

    render() {
        let item = this.props.cost;
        return (
            <TableRow>
                <TableCell>{item.sqlId}</TableCell>
                <TableCell>{item.timesTamp}</TableCell>
                <TableCell>{item.operation}</TableCell>
                <TableCell>{item.cost}</TableCell>
            </TableRow>
        );
    }
};